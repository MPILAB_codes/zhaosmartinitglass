/**  
# Flow in a rotating bottom-driven cylindrical container

We create a MArtini glass within a cylindrical system which is partially fillled with liquid

We use the two-phase axisymmetric Navier--Stokes solver with swirl. */

/*#include "grid/multigrid.h"
#include "navier-stokes/centered.h"
#include "two-phase.h"
#include "tension.h"
#include "embed.h"
#include "view.h"*/
//#include "grid/multigrid3D.h"  // (This is without mask)
#include "grid/octree.h"
#include "embed.h"
#include "navier-stokes/centered.h"
/*#include "view.h"*/
#include "two-phase.h"
#include "output3d.h"
//#include "sliceOf3D.h"
#include "save_data.h" 
//#include "vof-tracer-particles.h"
//#include "output_vtu_foreach2.h"

//#include "tension.h"
//#include "tracer.h" 



/*
This is an test from my student : for the 120° cone, 50% glycerin (60 mPa s) 2.4 Hz, 25 ml of fluid: (1.1g/mL) (8.2 mPa s)

1.5 mm stroke length produces a very smooth, slow 4-leaf.

3 mm stroke length produces mostly smooth baseball. 2.75 mm stroke still produces an almost symmetric 4-leaf.

4 mm stroke length produces zig-zag

5 mm stroke produces something like a transitioning pattern between zig zag and turbulent.

10 mm seems fully turbulent.*/

//Particles Pin, Pout;

int vtucount = 0;

double tEnd = 1.5;


#define AN 60.0    //Half angle of glass in degrees
#define AM 0.01   //Amplitude of the wall velocity (correspomnds to displacement in m of HALF the length !!)
#define OM 2.4      //Frequency of the oscillation (Hz)
#define VO 25      // Volume of fluid in mL
#define MU 8.2     // Viscosity of liquid (mPa s)


scalar T[],T2[];
scalar * tracers = {T,T2};

face vector av[];
//face vector av[];


double LH;

double G = 0.25,
       Fr = 0.88,
       We = 3153,
       rhor = 1.205/1.1e3,
       mur = 18.2e-6/(MU/1000.0),
       eps = 1.0e-5; 

int main() {
  size (0.1);
  init_grid (64);
  a = av;
  DT  = 0.00005;
  mu1  = MU/1000.0;   
  rho2 = rho1*rhor;
  mu2  = mu1*mur;
  eps = 0.02*L0;
  origin (0,-L0/2., -L0/2.);
  run();

}


event acceleration (i++)
{

  foreach_face(){
    av.x[] -= 9.8;
    av.y[] -= pow(2*pi*OM,2)*AM*sin(2*pi*OM*t);
   }
    
}


event init (i = 0) {
  LH=pow((((1E-6 * VO*3)/(pow(tan(AN*pi/180.0),2)*pi))),(1./3.));  /*Height of liquid*/
  //#((1E-6 * VO*3)/(((np.tan(AN*np.pi/180.0))**2)*np.pi))**(1/3)
  fraction (f, LH-x);
  //solid (cs, fs, (x*x)*pow(tan(AN*pi/180.0),2)-(z*z + y*y));
  mask ((x*x)*pow(tan(AN*pi/180.0),2)<(z*z + y*y) ? left : none);
  mask (x>0.04 ? top:none); 
 
  //mask ((x*x)/(tan(AN*pi/180.0)*tan(AN*pi/180.0))>(z*z + y*y) ? martini : none);
  /* A sphere is defined as y^2+z^2 = x^2/a  or f(x,y,z)=y^2+z^2 - x^2/a=0   ;
   The normal vector to the cone is (df/dx , df/dy, df/dz) = (-2x/a, 2y,+ 2z)
   To length of the vector is: L=(4x^2/a^2 + 4y^2 + 4z^2)^0.5
   The velocity vector of the wall is then Oscil = (0,Wall,0) in the direction of motion
   Oscil dot normal = 2y*Wall/L  
  */
  
  /*We can calculate the tangent in the x direction knowing that the cone passes by (0,0,0). As such (x,y,z)/sqrt(x^2+y^2+z^2) is the unit tangent unit vector along x). The component: Oscil dot tang = wall * y /sqrt(x^2+y^2+z^2)*/
  
  u.y[embed] = dirichlet (0.0);
  //#u.y[embed] = dirichlet (AM*sin(2*pi*OM*t));
  u.z[embed] = dirichlet (0.0);
  u.n[top]   = neumann(0.0);
  u.n[front] = neumann(0.0);
  u.n[back]  = neumann(0.0);
  u.n[left]  = neumann(0.0); 
  u.n[right]  = neumann(0.0); 
  foreach()
    if (x > LH-(eps) && x < LH+eps) {
	  if (y > -(eps) && y < eps) {
	     T[]=1.0;
	   }
	   if (y > L0/4-(eps) && y < L0/4+eps) {
	     T[]=1.0;
	   }

	   
	   if (z > -(eps) && z < eps) {
	     T2[]=1.0;
	   }    
	   if (z > L0/4-(eps) && z < L0/4+eps) {
	     T2[]=1.0;
	   }
        }
}


event graphs (i++) {
  stats s = statsf (u.y);
  stats s1 = statsf (u.x);
  fprintf (stderr, "%g %g %g  %g %g\n", t, s.min, s.max, s1.min, s1.max);
}



event velo (t += 0.01; t <= tEnd)
{
  char filename[80];
  sprintf (filename, "output-%03d.vtu", i);
  output_field_3d (all, fopen (filename, "w"));
}

event outputCycle (t = 3.2 ; t += 0.01; t <=3.75 )
{
  char filename[80];
  sprintf (filename, "fullCycle-%03d.vtu", i);
  output_field_3d (all, fopen (filename, "w"));
}


event end (i = tEnd) {
  printf ("i = %d t = %g, heightLiq= %g\n", dimension, t,LH);
}


