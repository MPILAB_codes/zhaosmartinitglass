# ZhaosMartinitGlass



## Getting started
This git repo is a folder to store the setup files for Zhao's martini glass project. This project is run on Basilisk.

##Running in parallel:

CC='mpicc -grid=multigrid3D -D_MPI=8' make ZhaosMartini.tst


On Niagara:
CC99='/usr/bin/mpicc -std=c99' qcc -Wall -grid=multigrid3D -O2 -D_MPI=1 ZhaosMartini.c -o mycode -lm
mpirun -np 8 ./mycode 

CC99='mpicc -std=c99' qcc -Wall -grid=multigrid3D -O2 -D_MPI=1 ZhaosMartini.c -o mycode -lm
mpirun -np 8 ./mycode

CC99='mpicc -std=c99' qcc -Wall -grid=octree -O2 -D_MPI=1 ZhaosMartini.c -o mycode -lm
mpirun -np 8 ./mycode

```
cd existing_repo
git remote add origin https://git.uwaterloo.ca/MPILAB_codes/zhaosmartinitglass.git
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
