import matplotlib.pyplot as plt
import numpy as np
import matplotlib.tri as tri
import sys
import pdb

plane=sys.argv[1]
numberfile=sys.argv[2]
leadingName="output"
#leadingName="fullCycle"
folder="ZhaosMartini"
data=np.loadtxt(folder+"/"+leadingName+"-"+numberfile+".vtu", unpack=True,comments='#')
refinedMesh=False

if len(numberfile)==3:
 numberfile="00"+numberfile
elif len(numberfile)==4:
 numberfile="0"+numberfile
print(np.shape(data))
# 1:x 2:y 3:z 4:cs 5:fs.x 6:fs.y 7:fs.z 8:p 9:u.x 10:u.y 11:u.z 12:g.x 13:g.y 14:g.z 15:pf 16:uf.x 17:uf.y 18:uf.z 19:f 20:alphav.x 21:alphav.y 22:alphav.z 23:rhov 24:myf 25:mu.x 26:mu.y 27:mu.z


x=data[0,:]
y=data[1,:]
z=data[2,:]

def calc_iso_surface(my_array, my_value, zs, interp_order=6, power_parameter=0.5, scalars=[],CS=[]):
    if interp_order < 1: interp_order = 1
    from numpy import argsort, take, clip, zeros
    dist = (my_array - my_value)**2
    arg = argsort(dist,axis=0)
    dist.sort(axis=0)
    w_total = 0.
    z = zeros(my_array.shape[:2], dtype=float)
    nx,ny=my_array.shape[:2]
    scalarray=np.zeros([nx,ny,len(scalars)])
    meanArg=int(np.mean(arg))

    for i in range(int(interp_order)):
        zi = take(zs, arg[i,:,:])
        valuei = dist[i,:,:]
        wi = 1/valuei
        clip(wi, 0, 1.e6, out=wi) # avoiding overflows
        w_total += wi**power_parameter
        z += zi*wi**power_parameter
        if len(scalars)>1:
            for j,myscal in enumerate(scalars):
              val=np.zeros(my_array.shape[:2])
              for ii in range(len(zs)):
                for jj in range(len(zs)):
                  val[ii,jj] = myscal[arg[i,ii,jj],ii,jj]
              scalarray[:,:,j] += val*wi**power_parameter
    z /= w_total
    if len(scalars)>1:
        for j,myscal in enumerate(scalars):
                scalarray[:,:,j] /= w_total
    if CS==[]:
        print("all good")
    else:
        for j,myscal in enumerate(scalars):
            hSurf=np.where(my_array[:,10,10] == 0)[0][0]
            scalarray[:,:,j]*=CS[0][hSurf,:,:]
    return z,scalarray,CS[0][hSurf,:,:]
    




Napprox=int(round((np.shape(data)[1])**(1/3)))

midPlane=0
eps=max(y)/Napprox

if plane=='z':
  arg=np.argwhere((z > midPlane-eps/10.) & (z < midPlane+eps*1.25))
  myGrid=y
elif plane=='y':
  arg=np.argwhere((y > midPlane-eps/10.) & (y < midPlane+eps*1.25))
  myGrid=z
elif plane=='x':
  X=np.reshape(x, [Napprox,Napprox,Napprox])
  Y=np.reshape(y, [Napprox,Napprox,Napprox])
  Z=np.reshape(z, [Napprox,Napprox,Napprox])
  f=np.reshape(data[18], [Napprox,Napprox,Napprox])
  U = np.reshape(data[8], [Napprox,Napprox,Napprox])
  V = np.reshape(data[9], [Napprox,Napprox,Napprox])
  W = np.reshape(data[10], [Napprox,Napprox,Napprox])
  CS= np.reshape(data[4], [Napprox,Napprox,Napprox])
  T= np.reshape(data[23], [Napprox,Napprox,Napprox])
  T2= np.reshape(data[24], [Napprox,Napprox,Napprox])
  zs=np.linspace(0,np.max(x),Napprox)
  myZ,scal,CSbound=calc_iso_surface(f, my_value=0.5, zs=zs, interp_order=6, power_parameter=0.5,scalars=[V,W,T,T2],CS=[CS])
  step=2
  
  myMin=-0.0006
  myMax=0.0006
  #% PLOT QUIVER MAP AT SURFACE!
  print("min/max: ",np.min(scal[:,:,1]),np.max(scal[:,:,1]))
  plt.contourf( Y[0,:,:],Z[0,:,:], scal[:,:,1],vmin=myMin,vmax=myMax)

  print("OUTPUT")
  plt.quiver( Y[0,::step,::step],Z[0,::step,::step], scal[::step,::step,0], scal[::step,::step,1])
  CSbound[CSbound == 1]=np.nan
  cmapp = plt.get_cmap('Greys')
  plt.contourf( Y[0,:,:],Z[0,:,:], CSbound,vmin=0.2,extend='neither',cmap=cmapp)
  plt.axis('equal')
  plt.xlim(-0.0375,0.0375)
  plt.ylim(-0.0375,0.0375)
  plt.savefig(folder+"/surface_"+plane+"_"+numberfile+".png")
  print("OUTPUT") 
  #%PLOT SURFACE HEIGHT !!
  plt.contourf( Y[0,:,:],Z[0,:,:], myZ-np.mean(myZ))
  plt.contourf( Y[0,:,:],Z[0,:,:], CSbound,vmin=0.2,extend='neither',cmap=cmapp)
  plt.axis('equal')
  plt.xlim(-0.0375,0.0375)
  plt.ylim(-0.0375,0.0375)
  plt.savefig(folder+"/surfaceHeight_"+plane+"_"+numberfile+".png")
  
  #% PLOT TRACER!
  T=scal[:,:,2]
  T2=scal[:,:,3]
  T[T < 0.0001]=np.nan
  T2[T2 < 0.0001]=np.nan
  plt.contourf( Y[0,:,:],Z[0,:,:], T,cmap='Blues',vmin=0, vmax=0.9)
  plt.contourf( Y[0,:,:],Z[0,:,:], T2,cmap='Reds',vmin=0, vmax=0.9)
  plt.contourf( Y[0,:,:],Z[0,:,:], CSbound,vmin=0.2,extend='neither',cmap=cmapp)
  plt.axis('equal')
  plt.xlim(-0.0375,0.0375)
  plt.ylim(-0.0375,0.0375)
  plt.savefig(folder+"/tracer_"+plane+"_"+numberfile+".png")
  quit()
  #plt.show()
  #z = calc_iso_surface( my_array, my_value=0.1, zs=zs, interp_order=6 )
  if refinedMesh:
      quit("you can't use refined mesh at this time")
  arg=np.argwhere((x > midPlane-eps/2.) & (x < midPlane+eps))

   



if refinedMesh:
  # Create grid values first.
  xi = np.linspace(np.min(x), np.max(x), int(Napprox*1.3))
  yi = np.linspace(np.min(myGrid), np.max(myGrid), int(Napprox*1.3))

  # Linearly interpolate the data (x, y) on a grid defined by (xi, yi).
  triang = tri.Triangulation(x[arg][:,0],myGrid[arg][:,0])
  interp_P = tri.LinearTriInterpolator(triang, data[7,arg][:,0])
  interp_U = tri.LinearTriInterpolator(triang, data[8,arg][:,0])
  interp_V = tri.LinearTriInterpolator(triang, data[9,arg][:,0])
  interp_W = tri.LinearTriInterpolator(triang, data[10,arg][:,0])
  interp_f = tri.LinearTriInterpolator(triang,data[18,arg][:,0])
  interp_CS = tri.LinearTriInterpolator(triang,data[4,arg][:,0])
  X,Y = np.meshgrid(xi, yi)
  Porg = interp_P(X, Y)
  Uorg = interp_U(X, Y)
  Vorg = interp_V(X, Y)
  Worg = interp_W(X, Y)
  f = interp_f(X, Y)
  CS = interp_CS(X, Y)
  print("Umax= %g, Vmax= %g \n",np.max(abs(data[8,arg][:,0])),np.max(abs(data[9,arg][:,0])))
else:
  X=np.reshape(x[arg], [Napprox,Napprox])
  Y=np.reshape(y[arg], [Napprox,Napprox])
  Z=np.reshape(z[arg], [Napprox,Napprox])
  f=np.reshape(data[18,arg], [Napprox,Napprox])
  CSorg= np.reshape(data[4,arg], [Napprox,Napprox])
  Porg = np.reshape(data[7,arg], [Napprox,Napprox])
  Uorg = np.reshape(data[8,arg], [Napprox,Napprox])
  Vorg = np.reshape(data[9,arg], [Napprox,Napprox])
  Worg = np.reshape(data[10,arg], [Napprox,Napprox])
cond1=CSorg >-0.50
cond2=f>-0.5
CS=np.copy(CSorg)



P=Porg
U=Uorg
V=Vorg
W=Worg


CS[CSorg == 1]=np.nan
CS[f > 0.55]=np.nan


cmapp = plt.get_cmap('Greys')



step=2
Uquiver=U
if plane=='z':
  Vquiver=V
  pdb.set_trace()
  plt.contourf(Y,X,f,cmap='jet')
  pdb.set_trace()
  #plt.quiver( Y[::step,::step],X[::step,::step], Uquiver[::step,::step], Vquiver[::step,::step])
  #plt.contourf(Y,X, f,vmin=0.2,extend='neither',cmap=cmapp)
  plt.contourf(Y,X,CS,vmax=0.2,extend='neither',cmap=cmapp)
  plt.axis('equal')
  plt.ylim([0,0.025])
  plt.xlim([-0.0375,0.0375])
elif plane=='y':
  Vquiver=W
  #plt.contourf(Y,X,V,cmap='jet')
  plt.contourf(Z,X,f,cmap='jet')
  #plt.quiver( Z[::step,::step],X[::step,::step], Uquiver[::step,::step], Vquiver[::step,::step])
  plt.contourf(Z,X, CS,vmin=0.2,extend='neither',cmap=cmapp)
  #plt.contourf(Z,X, f,vmax=0.2,extend='neither',cmap=cmapp)
  plt.axis('equal')
  plt.ylim([0,0.025])
  plt.xlim([-0.0375,0.0375])


plt.savefig(folder+"/slideMid_"+plane+"_"+numberfile+".png")
